<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Film;
use Illuminate\Support\Facades\Validator;

class FilmController extends Controller {
    function __construct() {
        // set session
        session(['username' => 'XXX']);
    }


    function save(Request $req) {
        //var_dump($req->all()); // all() return array of semua data dari form
        $film_id = $req->film_id;

        if(empty($film_id)) {
            // create
            $film = new Film();
        } else {
            // update
            $film = Film::find($film_id);
        }

        $film->title = $req->title;
        $film->description = $req->description;
        $film->release_year = $req->release_year;
        $film->language_id = 1;
        $film->rental_duration = 1;
        $film->original_language_id = 1;
        $film->length = 1;

        // validation
        $data = $req->all(); // return semua data dari form
        $rules = [
            'title'        => 'required',
            'description'  => 'required',
            'release_year' => 'required'
        ];
        $message= [
            'title.required'        => 'Tajuk wajib diisi',
            'description.required'  => 'Keterangan wajib diisi',
            'release.year.required' => 'Tahun keluaran wajib diisi'
        ];
        $v = Validator::make($data, $rules, $message);

        if ($v->fails()) {
            // validation gagal. show back the form
            return view('film.form', ['film' => $film])->withErrors($v);
        } else {
            // validation berjaya. save data, redirect
            $film->save();
            return redirect('/film-all')->with('msg', 'Rekod telah berjaya disimpan');
        }
    }

    function delete($film_id) {
        Film::find($film_id)->delete();
        return redirect('/film-all');
    }

    function edit($film_id) {
        // find the record
        $film = Film::find($film_id); // find() = search by primary key, return a record
        return view('film.form', ['film' => $film]);
    }


    function create() {
        $film = new Film();
        return view('film.form', ['film' => $film]);
    }

    // show all film
    public function showAll(Request $req) {
        session(['username' => 'John Doe']);

        if ($req->isMethod('post')) {
            // searching
            $title = $req->title; // dr. form
            $films = Film::where('title', 'like', "%$title%")->paginate(20);
        } else {
            // $films = Film::all();
            $films = Film::paginate(20);
        }

        // foreach ($films as $f) {
        //     echo $f->title . '<br>';
        // }
        return view('film.list', ['films' => $films]);
    }
}
