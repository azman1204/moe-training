<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    function listing($store_id) {
        $customers = Customer::where('store_id', $store_id)->paginate(20);
        return view('customer.list', ['customers' => $customers]);
    }
}
