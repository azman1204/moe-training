<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Film extends Model {
    public $table = 'film';
    public $timestamps = false; // configure tiada field created_at dan updated_at
    public $primaryKey = 'film_id'; // by default PK is "id"

    function language() {
        // hasOne(nama model, fk, pk)
        return $this->hasOne(\App\Models\Language::class, 'language_id', 'language_id');
    }

    function actor() {
        return $this->belongsToMany(\App\Models\Actor::class,
        'film_actor', 'film_id', 'actor_id');
    }
}
