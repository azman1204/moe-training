<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;
    public $table = 'store';

    function customer() {
        return $this->hasMany(\App\Models\Customer::class, 'store_id', 'store_id');
    }
}
