<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    public $table = 'customer';

    // 1 store has 1 address
    function address() {
        return $this->hasOne(\App\Models\Address::class, 'address_id', 'address_id');
    }
}
