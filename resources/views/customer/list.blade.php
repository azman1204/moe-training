@extends('layout.master')
@section('content')
    <form action="/customer-store" method="GET">
        <select name="store" id="store" class="form-control">
            <option value="0">-- Sila Pilih --</option>
            <option>1</option>
            <option>2</option>
        </select>
    </form>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
            @php $no = $customers->firstItem() @endphp
            @foreach ($customers as $cust)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $cust->first_name }}</td>
                    <td>{{ $cust->email }}</td>
                    <td>{{ $cust->address->address }} {{ $cust->address->district }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $customers->links() }}
@endsection

@section('js')
<script>
$(function() {
    $('#store').change(function() {
        var id_store = $(this).val();
        location.href='/customer-store/' + id_store; // redirect
    });
});
</script>
@endsection
