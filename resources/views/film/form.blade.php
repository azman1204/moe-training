@extends('layout.master')
@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $message)
            {{ $message }} <br>
        @endforeach
    </div>
@endif

<form action="/film-save" method="POST" id="myform">
    <input type="hidden" name="film_id" value="{{ $film->film_id }}">
    @csrf
    <div class="row">
        <div class="col-md-1">Title</div>
        <div class="col-md-11">
            <input value="{{ $film->title }}" type="text" name="title" class="form-control"
            required data-msg-required='Tajuk wajib diisi'>
            @error('title')
                <label class="text-danger">{{ $message }}</label>
            @enderror
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-md-1">Description</div>
        <div class="col-md-11">
            <textarea name="description" class="form-control"
            cols="30" rows="10">{{ $film->description }}</textarea>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-md-1">Release Year</div>
        <div class="col-md-11">
            <input value="{{ $film->release_year }}" type="text" name="release_year" class="form-control">
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11"><input type="submit" vlue="Save" class="btn btn-primary"></div>
    </div>
</form>
@endsection

@section('js')
<script>
    $('#myform').validate();
</script>
@endsection
