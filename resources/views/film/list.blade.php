@extends('layout.master')
@section('title', 'Film Listing')

@section('sidebar')
    @parent
    this will append to sidebar
@endsection

@section('content')
    <form action="/film-all" method="POST">
        @csrf
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-1">Title</div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="title">
                    </div>
                    <div class="col-md-1">
                        <input type="submit" class="btn btn-primary" value="Cari">
                    </div>
                </div>
            </div>
        </div>
    </form>

    <a href="/film-create" class="btn btn-primary btn-sm">New Film</a>
    <br>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Title</th>
                <th>Description</th>
                <th>Release Year</th>
                <th>Language</th>
                <th>Actor</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @php $no = $films->firstItem() @endphp
            @foreach($films as $f)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $f->title }}</td>
                <td>{{ $f->description }}</td>
                <td>{{ $f->release_year }}</td>
                <td>{{ $f->language->name }}</td>
                <td>
                   @foreach ($f->actor as $act)
                        {{ $act->first_name }} <br>
                   @endforeach
                </td>
                <td>
                    <a href="/film-edit/{{ $f->film_id }}"
                        class="btn btn-primary btn-sm">Edit</a>

                    <a href="/film-delete/{{ $f->film_id }}"
                        class="btn btn-danger btn-sm" onclick="return confirm('Anda Pasti ?')">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $films->links() }}
@endsection
