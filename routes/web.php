<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\CustomerController;

Route::get('/customer-store/{store_id}', [CustomerController::class, 'listing']);

Route::get('/user/{id}', [UserController::class, 'show']);
Route::any('/film-all', [FilmController::class, 'showAll']);
Route::get('/film-create', [FilmController::class, 'create']);
Route::post('/film-save', [FilmController::class, 'save']);
Route::get('/film-edit/{film_id}', [FilmController::class, 'edit']);
Route::get('/film-delete/{film_id}', [FilmController::class, 'delete']);


// http://moe-training.test/hello/ali
// http://localhost/moe-training/public/hello/ali
Route::get('/hello/{nama?}', function ($nama = 'John Doe') {
    return view('welcome', ['nama' => $nama, 'alamat'=> 'Bangi']);
});

// 5 http protocol: 1. get 2. post 3. put 4. delete 5. patch
// http://localhost/moe-training/public/hello-world
Route::get('/hello-world', function() {
    return 'Hello World';
});

// http://localhost/moe-training/public/welcome
Route::view('/welcome', 'welcome', ['nama' => 'Azman']); // resources/views/welcome.blade.php
